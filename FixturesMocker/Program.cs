﻿using System;
using System.IO;
using System.Linq;
using System.Net;

namespace FixturesMocker
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = args[0];
            string[] urlList = args[2].Split('|');

            var urls = urlList.ToList();

            foreach(string url in urls)
            {
                WebRequest req = WebRequest.Create(url);
                req.Method = "POST";
                req.ContentType = "application/xml";
                req.Headers.Add("x-meta-default-filename", "file2.xml");

                var writer = new StreamWriter(req.GetRequestStream());
                writer.WriteLine(GetTextFromXmlFile(fileName));
                writer.Close();

                WebResponse rsp = req.GetResponse();
                req.GetRequestStream().Close();
                {
                    var responseStream = rsp.GetResponseStream();
                    if (responseStream != null) responseStream.Close();
                }
            }

            Console.ReadKey();
        }

        private static string GetTextFromXmlFile(string file)
        {
            var reader = new StreamReader(file);
            string ret = reader.ReadToEnd();
            reader.Close();
            return ret;
        }
    }
}
